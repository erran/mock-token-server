#### Usage

Pre-requisites
1. Clone this repository and navigate to the root folder
1. Install https://github.com/FiloSottile/mkcert
   - e.g. `brew install mkcert`
1. Create local certificate pair using `mkcert`
1. Install the root CA with `mkcert -install`
1. Generate a local certificate/key pair with `mkcert localhost`
1. Ensure that the paths in `main.js` point to the new files.

Running the app

1. Run `npm install`
1. Run the app with `node main.js`
