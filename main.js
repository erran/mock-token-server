var fs = require("fs");
var http = require("http");
var https = require("https");

var privateKey = fs.readFileSync("./localhost-key.pem", "utf8");
var certificate = fs.readFileSync("./localhost.pem", "utf8");

var credentials = { key: privateKey, cert: certificate };
var express = require("express");
var app = express();

app.get("/", (req, res) => {
  // just for the testing purpose
  res.send("Hello World");
});

// Mock the token request (it is the first request that is made when you setup the account in VSCode and the first one on the LS side as well)
app.get("/api/v4/personal_access_tokens/self", (req, res) => {
  const jsonResponse = {
    scopes: ["api", "read_api"],
    active: true,
  };

  console.log("/api/v4/personal_access_tokens/self");

  res.json(jsonResponse);
});

// Mock the user request (it is the second request that is made when you setup the account in VSCode)
app.get("/api/v4/user", (req, res) => {
  const jsonResponse = {
    id: "1",
    username: "user",
  };

  console.log("/api/v4/user");

  res.json(jsonResponse);
});

// var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

// httpServer.listen(8080);
// https://localhost:8443
httpsServer.listen(8443);

console.log("Listening to https://localhost:8443");
